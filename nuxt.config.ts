// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    compatibilityDate: '2024-04-03',
    devtools: {enabled: true},
    future: {
        compatibilityVersion: 4
    },
    modules: [
        "@nuxt/ui",
        "@nuxtjs/i18n",
        "@nuxtjs/apollo"
    ],
    ssr: false,

    i18n: {
        strategy: "no_prefix",
        detectBrowserLanguage: {
            useCookie: true,
            cookieKey: "i18n",
            cookieCrossOrigin: true,
        },
        lazy: true,
        compilation: {
            strictMessage: false,
        },
        locales: [
            {
                code: "en",
                name: "English",
                file: "en.json",
            },
            {
                code: "de",
                name: "Deutsch",
                file: "de.json",
            },
        ],
    },


    apollo: {
        clients: {
            default: {
                tokenStorage: "localStorage",
                tokenName: "citadel",
                httpEndpoint:
                    process.env.GRAPHQL_ENDPOINT_SERVER
                    || "http://127.0.0.1:3001/v0/graphql",
                browserHttpEndpoint:
                    process.env.GRAPHQL_ENDPOINT_CLIENT
                    || "http://127.0.0.1:3001/v0/graphql",
            },
        },
        cookieAttributes: {
            // For local connections
            secure: false,
        },
    },
})
