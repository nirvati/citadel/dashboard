// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

export default defineAppConfig({
  ui: {
    colors: {
      primary: "citadel",
    },
    toast: {
      slots: {
        root: "z-100"
      },
    },
    card: {
      slots: {
        root:  "bg-white dark:bg-zinc-900",
      },
    },
    input: {
      color: {
        white: {
          outline: "dark:bg-zinc-900",
        },
      },
    },
    navigationMenu: {
      variants: {
        active: {
          true: {
            childLink: "dark:before:bg-neutral-800",
          },
          false: {
            link: "dark:hover:before:bg-neutral-800/50",
          },
        },
      },
      compoundVariants: [
        {
          color: 'primary',
          variant: 'pill',
          active: true,
          class: {
            link: 'dark:text-white dark:before:bg-neutral-800',
            linkLeadingIcon: 'dark:text-white'
          }
        },
        {
          color: 'primary',
          variant: 'link',
          active: true,
          class: {
            link: 'dark:text-white dark:before:bg-neutral-800',
            linkLeadingIcon: 'dark:text-white'
          }
        },
      ]
    },
  },
});
