/* eslint-disable */
// This file is generated automatically. DO NOT EDIT IT MANUALLY.
import type { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  /** A scalar that can represent any JSON Object value. */
  JSONObject: { input: { [key: string]: any }; output: { [key: string]: any }; }
};

export type Bitcoin = {
  __typename?: 'Bitcoin';
  syncState: SyncState;
};

export type Config = {
  __typename?: 'Config';
  parsed: Scalars['JSONObject']['output'];
  raw: Scalars['String']['output'];
};

export type Mutation = {
  __typename?: 'Mutation';
  configAdd: Scalars['Boolean']['output'];
  configRemove: Scalars['Boolean']['output'];
  configSet: Scalars['Boolean']['output'];
  initWallet: Scalars['Boolean']['output'];
  restartLnd: Scalars['Boolean']['output'];
};


export type MutationConfigAddArgs = {
  key: Scalars['String']['input'];
  section: Scalars['String']['input'];
  value: Scalars['String']['input'];
};


export type MutationConfigRemoveArgs = {
  key: Scalars['String']['input'];
  section: Scalars['String']['input'];
};


export type MutationConfigSetArgs = {
  key: Scalars['String']['input'];
  section: Scalars['String']['input'];
  value: Scalars['String']['input'];
};


export type MutationInitWalletArgs = {
  seed: Array<Scalars['String']['input']>;
};

export type Query = {
  __typename?: 'Query';
  bitcoin: Bitcoin;
  config: Config;
  user: User;
  wallet: Wallet;
};

export type SyncState = {
  __typename?: 'SyncState';
  chainTip: Scalars['Int']['output'];
  syncedTo: Scalars['Int']['output'];
  verificationProcess: Scalars['Float']['output'];
};

export type User = {
  __typename?: 'User';
  email?: Maybe<Scalars['String']['output']>;
  username: Scalars['String']['output'];
};

export type Wallet = {
  __typename?: 'Wallet';
  isInitialized: Scalars['Boolean']['output'];
  randomSeed: Array<Scalars['String']['output']>;
};

export type SyncInfoQueryVariables = Exact<{ [key: string]: never; }>;


export type SyncInfoQuery = { __typename?: 'Query', bitcoin: { __typename?: 'Bitcoin', syncState: { __typename?: 'SyncState', verificationProcess: number, chainTip: number, syncedTo: number } } };

export type GetConfigQueryVariables = Exact<{ [key: string]: never; }>;


export type GetConfigQuery = { __typename?: 'Query', config: { __typename?: 'Config', parsed: { [key: string]: any } } };

export type GetMeQueryVariables = Exact<{ [key: string]: never; }>;


export type GetMeQuery = { __typename?: 'Query', wallet: { __typename?: 'Wallet', isInitialized: boolean } };

export type GetSeedQueryVariables = Exact<{ [key: string]: never; }>;


export type GetSeedQuery = { __typename?: 'Query', wallet: { __typename?: 'Wallet', randomSeed: Array<string> } };

export type InitWalletMutationVariables = Exact<{
  seed: Array<Scalars['String']['input']> | Scalars['String']['input'];
}>;


export type InitWalletMutation = { __typename?: 'Mutation', initWallet: boolean };


export const SyncInfoDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"syncInfo"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"bitcoin"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"syncState"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"verificationProcess"}},{"kind":"Field","name":{"kind":"Name","value":"chainTip"}},{"kind":"Field","name":{"kind":"Name","value":"syncedTo"}}]}}]}}]}}]} as unknown as DocumentNode<SyncInfoQuery, SyncInfoQueryVariables>;
export const GetConfigDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"getConfig"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"config"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"parsed"}}]}}]}}]} as unknown as DocumentNode<GetConfigQuery, GetConfigQueryVariables>;
export const GetMeDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"getMe"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"wallet"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"isInitialized"}}]}}]}}]} as unknown as DocumentNode<GetMeQuery, GetMeQueryVariables>;
export const GetSeedDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"getSeed"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"wallet"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"randomSeed"}}]}}]}}]} as unknown as DocumentNode<GetSeedQuery, GetSeedQueryVariables>;
export const InitWalletDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"initWallet"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"seed"}},"type":{"kind":"NonNullType","type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"initWallet"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"seed"},"value":{"kind":"Variable","name":{"kind":"Name","value":"seed"}}}]}]}}]} as unknown as DocumentNode<InitWalletMutation, InitWalletMutationVariables>;