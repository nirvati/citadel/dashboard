// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

function toast(data: any) {
  window.top?.postMessage({
    action: "toast",
    data,
  }, "*");
}

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.hook("apollo:error", (error) => {
    // const { t } = useI18n();
    for (const graphQLError of error.graphQLErrors || []) {
      if (graphQLError.message === "Variable id is not defined.") {
        console.warn(graphQLError);
        return;
      }
      console.error(graphQLError);
      toast({
        title: /* t("error.graphql.title") */ "Internal error",
        description: graphQLError.message,
        color: "error",
        icon: "i-heroicons-exclamation-circle",
      });
    }
    if (error.networkError) {
      console.error(error.networkError);
      toast({
        title: /* t("error.network.title") */ "Network error",
        description: error.networkError.message,
        color: "error",
        icon: "i-heroicons-exclamation-circle",
      });
    }
  });
});
