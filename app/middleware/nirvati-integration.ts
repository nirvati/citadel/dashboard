export default defineNuxtRouteMiddleware((to) => {
    // skip middleware on server
    if (import.meta.server) return;
    window?.top?.postMessage({action: 'navigate', data: to.fullPath }, '*');
});
