# SPDX-FileCopyrightText: 2024 The Nirvati Developers
#
# SPDX-License-Identifier: AGPL-3.0-or-later

FROM oven/bun:1 AS build

WORKDIR /build
COPY . .

RUN bun install

# This is a client-side app only currently
ENV GRAPHQL_ENDPOINT_CLIENT /api/v0/graphql

RUN NITRO_PRESET=bun bun run build

RUN rm -rf .output/server/node_modules

RUN cd .output/server && \
    bun install

FROM oven/bun:1-distroless

WORKDIR /app
COPY --from=build /build/.output .

EXPOSE 3000
CMD [ "./server/index.mjs" ]
